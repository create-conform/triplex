/////////////////////////////////////////////////////////////////////////////////////////////
//
// Triplex
//
//    Node.js server framework for serving frontend, api and backend.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
///////////////////////////////////////////////////////////////////////////////////////////// 
//
// Privates
//
///////////////////////////////////////////////////////////////////////////////////////////// 
var config =       require("config");
var ioURI =        require("io-uri");
    Error =        require("error");
var event =        require("event");


var CONFIG_PATH =  "triplex/triplex.json";
var CONFIG_DEFAULT = {
    "authentication" : {
        "ntlm" : {
            "enable" : false
        }
    },
    "modules" : [
        { 
            "triplex-endpoint" : {}
        }
    ]
};


/////////////////////////////////////////////////////////////////////////////////////////////
//
// Triplex Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function Triplex(options) {
    if (!(this instanceof Triplex)) {
        var t = new Triplex(options);
        t.start();

        //DEBUG
        //setTimeout(t.stop, 2000);

        return t;
    }

    var self = this;
    var modules = [];
    var stopping = 0;
    var shared = {
        "config" : CONFIG_DEFAULT,
        "endpoints" : {
            "add" : function(name, endpoint) {
                shared.endpoints[name] = endpoint;
                endpointEvents.fire(Triplex.prototype.EVENT_ENDPOINT_ADDED, { "name" : name, "endpoint" : endpoint });
    
                console.log("Added endpoint '" + name + "'.");
            },
            "remove" : function(name) {
                delete shared.endpoints[name]
                endpointEvents.fire(Triplex.prototype.EVENT_ENDPOINT_REMOVED, { "name" : name });
    
                console.log("Removed endpoint '" + name + "'.");
            }
        },
        "db" : {
            "add" : function(name, db) {
                shared.db[name] = db;
                dbEvents.fire(Triplex.prototype.EVENT_DB_ADDED, { "name" : name, "db" : db });
    
                console.log("Added db '" + name + "'.");
            },
            "remove" : function(name) {
                delete shared.db[name]
                dbEvents.fire(Triplex.prototype.EVENT_DB_REMOVED, { "name" : name });
    
                console.log("Removed db '" + name + "'.");
            }
        }
    };
    var endpointEvents;
    var dbEvents;
    
    
    ////////////////////////////////////////////////////////////////////
    //
    // Add Event Emitter To Endpoints Class
    //
    ////////////////////////////////////////////////////////////////////
    endpointEvents = new event.Emitter(shared.endpoints);
    dbEvents =       new event.Emitter(shared.db);


    ////////////////////////////////////////////////////////////////////
    //
    // Init
    //
    ////////////////////////////////////////////////////////////////////
    function init() {
        // load all modules, and instantiate
        for (var m in shared.config.modules) {
            for (var n in shared.config.modules[m]) {
                try {
                    modules.push({ 
                        "name" : n,
                        "instance" : require(n)(shared.config.modules[m][n], shared)
                    });
                }
                catch(e) {
                    console.error("Can't initialize '" + n + "'.", e);
                }
            }
        }

        // start all modules
        for (var m in modules) {
            try {
                startModule(m);
            }
            catch(e) {
                console.error("Can't start '" + modules[m].name + "'.", e);
            }
        }
    }


    ////////////////////////////////////////////////////////////////////
    //
    // Invokes module start
    //
    ////////////////////////////////////////////////////////////////////
    function startModule(m) {
        modules[m].instance.start().then(function() {
            console.log(  "Started module '" + modules[m].name + "'.");
        }).catch(function(e) {
            console.error("Failed to start module '" + modules[m].name + "'.", e);
        });
    }


    ////////////////////////////////////////////////////////////////////
    //
    // Load Configuration
    //
    ////////////////////////////////////////////////////////////////////
    function loadConfig() {
        return config.load(CONFIG_PATH)
        .then(function(conf) {
            shared.config = conf;
            try {
                init();
            }
            catch(e) {
                console.error(e);
            }
        })
        .catch(function(e) {
            if (e && e.name != ioURI.ERROR_NO_ENTRY) {
                console.error(e);
            }
            return config.save(shared.config, CONFIG_PATH)
            .then(function() {
                try {
                    init();
                }
                catch(e) {
                    console.error(e);
                }
            })
            .catch(function(e) {
                console.warn(self.ERROR_CANT_WRITE_CONFIG, e);
                try {
                    init();
                }
                catch(e) {
                    console.error(e);
                }
            });
        });
    }


    ////////////////////////////////////////////////////////////////////
    //
    // Service Controllers
    //
    ////////////////////////////////////////////////////////////////////
    this.start = function() {
        if (options && options.constructor === Object && Object.keys(options).length > 0) {
            shared.options = options;
            return init();
        }

        loadConfig();
    };

    this.stop = function() {
        if (stopping) throw new Error(self.ERROR_SHUTTING_DOWN, "The server is executing a shutdown procedure.");
        if (!modules.length) return true;

        stopping = true;

        modules[0].instance.stop().then(function() {
            console.log("Stopped module '" + modules[0].name + "'.");
            modules.splice(0,1);

            stopping = false;

            self.stop();
        }).catch(function(e) {
            console.error("Failed to stop module '" + modules[0].name + "'.", e);

            modules.splice(0,1);
            
            stopping = false;

            self.stop();
        });
    };
};
Triplex.prototype.EVENT_ENDPOINT_ADDED =    "add";
Triplex.prototype.EVENT_ENDPOINT_REMOVED =  "remove";
Triplex.prototype.EVENT_DB_ADDED =          "add";
Triplex.prototype.EVENT_DB_REMOVED =        "remove";
Triplex.prototype.ERROR_CANT_WRITE_CONFIG = "Can't Write Config";
Triplex.prototype.ERROR_SHUTTING_DOWN =     "Shutting Down";


/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = Triplex;
# triplex

Modular server, intended for serving backend, api & frontend in node.js.
It allows for easy configuration and installation of modules.
Triplex was built to solve the problem of sharing resources, such as endpoints and databases. An example use case could be that you host multiple websites, spread over multiple modules on the same port. That way, when you update a website, you only need to update the module and not triplex itself.

## Installation

```sh
npm install -g triplex
```

## Usage

```
triplex [options]

Options:
   --config-file    <path>   The path to a json file that contains parameters for the modules.
   --config         <json>   A JSON object with parameters for the modules.
```

## Confiugration & Installing Modules

First you need to install the module into the global npm repository.

```sh
npm install -g triplex-hsp
```

Once the module is installed, you need to add it to the triplex configuration. You can either store configuration in a json file, or pass the json object directly on the terminal/commandline by using the "--config" option.

When you choose a file to save and load your configuration, you have the option to specify the path to the configuration file by using the "--config-file" option, or by using the default location that is used when you start triplex without ay of the config options.
The location of the config file depents on the OS. A default configuration file is created the first time you run triplex.

| OS | Location |
| ---- | ----- |
| Windows | %APPDATA%\triplex\triplex.json |
| macOS | ~/Library/Preferences/triplex/triplex.json |
| linux | ~/.config/triplex/triplex.json |


```json
{
    "modules": [
        {
            "triplex-endpoint": {}
        },
        {
            "triplex-hsp" : {
                "path" : "./"
            }
        }
    ]
}
```

In the example above we added the "triplex-hsp" module that will serve pages from the current working directory (that is the directory you are in when you launch triplex from the terminal or commandline.

*Note*
If you are running triplex from another location than the npm global repository, then you need to export the global repository path as an environment variable.
ex. For bash on Windows, run this command:
```
export NODE_PATH=$APPDATA\\npm\\node_modules
```

## Available Modules

At the time of writing, there are a few modules available already in the npm repository.
Click on the gitlab link to see the module documentation for more information.

| Package | Description |
| ------- | ----------- |
| triplex-endpoint | Is installed by default when you install triplex. It is the module that allows you to create endpoints, which are required for other modules to serve data to a browser, ... |
| triplex-hsp | Handlebars server pages implementation. Think of it as good old asp, but with javascript and handlebars. |
| triplex-mongodb | This module allows you to add mongodb connections that can be used by the other modules |
| triplex-dispatcher | For dispatching jobs to nodes. | 
| triplex-filesystem-watcher | For monitoring file system changes and triggering jobs. | 

## Creating Custom Modules

You can create custom modules to perform specific tasks. A good example can be found in the triplex-hsp documentation. There, a module is created that needs serves some handlebars files, that need to access an internal api.

The basic requirements for creating a custom module is very simple. It needs to export a function that needs to be instantiated with "new", and needs to have a start and stop function.

When this module is installed globally using npm, you can add the module to the triplex configuration, with options. triplex will then instantiate the class in the order they appear in the config, and pass two parameters, "options" and "shared".

### Options Object

This object comes straight from the triplex config file.

### Shared Object

This is an object that is shared amongst all modules. It contains endpoints and database connections when they are defined in the triplex config.

```
shared
    .config       Contains the triplex configuration.
    .db           Contains database connections that can be used by all modules. It
                  is an object with named properties.
    .endpoints    Contains express endpoints that can be used by all modules. this, also
                  is an object with named properties.
```